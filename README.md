## Electruxt

A [Nuxt](https://nuxtjs.org/) + [Electron](https://electron.atom.io/) template

Supports:
- Packaging with electron-builder
- Linting with ESlint
- Testing with mocha and spectron
- CI with gitlab

# Installation

```bash
npm i -g vue-cli
```
```bash
vue init gitlab:rovergames/electruxt project-name
```

# Error when installing

If you have an error installing the template, you may edit vue-cli/bin/vue-init at line 68, change to this:
```js
const tmp = path.join(home, '.vue-templates', template.replace(/[\/|:]/g, '-'))
```