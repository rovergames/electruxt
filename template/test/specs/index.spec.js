import utils from '../utils';

describe('Launch', function () {
    beforeEach(utils.beforeEach);
    afterEach(utils.afterEach);

    it('shows an initial window', function () {
        return this.app.client.getWindowCount().then(function (count) {
            assert.equal(count, 1);
        });
    });
});

describe('Route /', function () {
    beforeEach(utils.beforeEach);
    afterEach(utils.afterEach);

    it('renders name application as title', function () {
        return this.app.client.getText('h1.title')
            .then(title => assert.equal(title, '{{ name }}'));
    });
});
