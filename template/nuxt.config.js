module.exports = {
    /*
    ** Electron Settings
    */
    electron: {
        width: 1024,
        height: 768
    },
    build: {
        extend (config, { isClient }) {
            if (isClient) {
                config.target = 'electron-renderer';
            }
        },
        vendor: ['vue-electron'],
    },
    plugins: ['~/plugins/vue-electron'],
}
