module.exports = {
  root: true,
  parser: 'babel-eslint',
  env: {
    browser: true,
    node: true
  },
  {{#if_eq eslintConfig 'standard'}}
  extends: 'standard',
  {{/if_eq}}
  {{#if_eq eslintConfig 'airbnb'}}
  extends: 'airbnb-base',
  {{/if_eq}}
  {{#if_eq eslintConfig 'rovergames'}}
  extends: 'rovergames',
  {{/if_eq}}
  // required to lint *.vue files
  plugins: [
    'html'
  ],
}
