# {{ name }}

> {{ description }}

## Installation

Install the dependencies:
```bash
npm install # Or yarn install
```

## Development mode

```bash
npm run dev
```

## Test

```bash
npm run test
```

## Production mode

```bash
npm run build
npm run start
```

## Package the app

```bash
npm run pack
```

See more on [electron-builder documentation](https://www.electron.build/).