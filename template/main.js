/*
**  Nuxt.js part
*/
process.env.NODE_ENV = process.env.NODE_ENV || 'production';
let win = null; // Current window

const http = require('http');
const { Nuxt, Builder } = require('nuxt');

// Import and Set Nuxt.js options
let config = require('./nuxt.config.js');
config.dev = !(process.env.NODE_ENV === 'production');
config.rootDir = __dirname; // for electron-packager

// Init Nuxt.js
const nuxt = new Nuxt(config);
const builder = new Builder(nuxt);
const server = http.createServer(nuxt.render);

// Build only in dev mode
if (config.dev) {
    builder.build()
        .catch((error) => {
            console.error(error); // eslint-disable-line
            process.exit(1);
        })

    require('electron-debug')({ showDevTools: true }); // eslint-disable-line

    require('electron').app.on('ready', () => { // eslint-disable-line
        const installExtension = require('electron-devtools-installer'); // eslint-disable-line
        installExtension.default(installExtension.VUEJS_DEVTOOLS)
            .then(() => {})
            .catch(err => {
                console.log('Unable to install `vue-devtools`: \n', err);
            });
    });
}

// Listen the server
server.listen();
const NUXT_URL = `http://localhost:${server.address().port}`;
console.log(`Nuxt working on ${NUXT_URL}`);

/*
** Electron app
*/
const electron = require('electron'); // eslint-disable-line
const path = require('path');
const url = require('url');

const POLL_INTERVAL = 300;
const pollServer = () => {
  http.get(NUXT_URL, (res) => {
    const SERVER_DOWN = res.statusCode !== 200;
    SERVER_DOWN ? setTimeout(pollServer, POLL_INTERVAL) : win.loadURL(NUXT_URL); // eslint-disable-line
  })
  .on('error', pollServer);
}

const app = electron.app;
const BW = electron.BrowserWindow;

const newWin = () => {
  win = new BW({
    width: config.electron.width || 800,
    height: config.electron.height || 600,
  });

  if (!config.dev) {
    win.loadURL(NUXT_URL);
    return;
  }

  win.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }));

  win.on('closed', () => {
    win = null;
  });

  pollServer();
}

app.on('ready', newWin);
app.on('window-all-closed', () => app.quit());
app.on('activate', () => win === null && newWin());
