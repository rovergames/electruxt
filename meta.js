module.exports = {
    "skipInterpolation": ["src/**/*.vue", "node_modules/**"],
    prompts: {
        name: {
            type: 'string',
            required: true,
            message: 'Application Name'
        },
        description: {
            type: 'string',
            required: false,
            message: 'Project description',
            default: 'An nuxt electron project'
        },
        eslint: {
            type: 'confirm',
            require: true,
            message: 'Use linting with ESLint?',
            default: true
        },
        eslintConfig: {
            when: 'eslint',
            type: 'list',
            message: 'Which eslint config would you like to use?',
            choices: [
                {
                    name: 'rovergames (https://gitlab.com/rovergames/eslint-config-rovergames)',
                    value: 'rovergames',
                    sort: 'rovergames'
                },
                {
                    name: 'Standard (https://github.com/feross/standard)',
                    value: 'standard',
                    short: 'Standard'
                },
                {
                    name: 'AirBNB (https://github.com/airbnb/javascript)',
                    value: 'airbnb',
                    short: 'AirBNB'
                },
                {
                    name: 'none (configure it yourself)',
                    value: 'none',
                    short: 'none'
                }
            ]
        },
    },
    complete (data, {logger, chalk}) {
        if (!data.inPlace) {
            logger.log(`cd ${chalk.yellow(data.destDirName)}`);
            logger.log(`npm i`);
        }
    },
}